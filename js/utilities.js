'use strict';
/**
 * change the color of a selector based on the color
 * that was given
 * @param {Element} inputSelector 
 * @param {Element} inputColor 
 */
function changeTextColor(inputSelector, inputColor) {
    inputSelector.style.color = inputColor;
}
/**
 * change the background color of a selector
 * based on the color that was given as input
 * @param {Element} inputSelector 
 * @param {Element} color 
 */
function changeBackground(inputSelector, color) {
    inputSelector.style.backgroundColor = color;
}
/**
 * change the selector width based on the parameter
 * inputed by the function 
 * @param {Element} inputSelector 
 * @param {Element} inputWidth 
 */
function changeTagWidth(inputSelector, inputWidth) {
    inputSelector.style.width = inputWidth + "%";
}
/**
 * Change the selector's border color based on the parameter color
 * @param {Element} inputSelector 
 * @param {Element} inputColor 
 */
function changeBorderColor(inputSelector, inputColor) {
    inputSelector.style.borderColor = inputColor;
}
/**
 * Change the selector's border width based on the parameter width given
 * @param {Element} inputSelector 
 * @param {Element} inputWidth 
 */
function changeBorderWidth(inputSelector, inputWidth) {
    inputSelector.style.borderWidth  = inputWidth + "px";
}