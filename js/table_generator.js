'use strict';

/** Function that creates a table dynamically based on parameters given. 
 * Also display the text area representing the code of the table
 * @param {number} rows - represent the value that the rows input have
 * @param {number} cols - represent the value that the column input have
  */
function genTable(rows, cols) {
    //Declaration of variable
    let table = document.createElement("table");
    let tableSection = document.getElementById("table-render-space");
    let tableBody = document.createElement("tbody");

    let tableWidthValue = document.getElementById("table-width");

    let txtColor = document.getElementById("text-color");

    let bgColor = document.getElementById("background-color");

    let borderWidthValue = document.getElementById("border-width");

    let borderColorValue = document.getElementById("border-color");

    tableSection.innerHTML = "";

    let textAreaFinder = document.getElementsByTagName('textarea');
    //if-statement that removes the textarea when the "previous" textarea exists
    if (textAreaFinder.length > 0) {
        textAreaFinder[0].remove();
    }

//nested for loop that runs throw the number of rows and columns to create the table
    for (let i = 0; i < rows.value; i++) {
        let tr = document.createElement("tr");
        for (let j = 0; j < cols.value; j++) {
            let td = document.createElement("td");
            td.appendChild(document.createTextNode("cell" + i + j));
            tr.appendChild(td);


            changeBorderColor(td, borderColorValue.value);
            changeBorderWidth(td, borderWidthValue.value);

            borderWidthValue.addEventListener("blur", function () {
                borderWidthValue = document.getElementById("border-width");
                changeBorderWidth(td, borderWidthValue.value);
            });
            borderColorValue.addEventListener("blur", function () {
                borderColorValue = document.getElementById("border-color");
                changeBorderColor(td, borderColorValue.value);
            });
        }
        tableBody.appendChild(tr);
    }
    table.appendChild(tableBody);
    tableSection.appendChild(table);

    changeBackground(table, bgColor.value);
    changeBorderColor(table, borderColorValue.value);
    changeTextColor(table, txtColor.value);
    changeTagWidth(table, tableWidthValue.value);
    changeBorderWidth(table, borderWidthValue.value);

    txtColor.addEventListener("blur", function () {
        txtColor = document.getElementById("text-color");
        changeTextColor(table, txtColor.value);
    });

    bgColor.addEventListener("blur", function () {
        bgColor = document.getElementById("background-color");
        changeBackground(table, bgColor.value);
    });

    tableWidthValue.addEventListener("blur", function () {
        tableWidthValue = document.getElementById("table-width");
        changeTagWidth(table, tableWidthValue.value);
    });

    rows.addEventListener('blur', function () {
        rows.addEventListener('blur', function () {
            rows = getRows();
        });
        genTable(rows, cols);
    });
    cols.addEventListener('blur', function () {
        cols.addEventListener('blur', function () {
            cols = getCols();
        });
        genTable(rows, cols);
    });
    createTextArea();
}
/**
 * get the number of rows based of the input in the forms
 * @returns {number} The value of rows 
 */
function getRows() {
    let rows = document.getElementById("row-count");
    return rows;
}
/**
 * get the number of columns based of the input in the forms
 * @returns {number} The value of columns
 */
function getCols() {
    let cols = document.getElementById("col-count");
    return cols;
}
/**
 * Creates the necessary variables and function calls to 
 * add information to the text area about the dynamically created table
 */
function createTextArea() {
    let textArea = document.createElement("textarea");
    let textAreaSection = document.getElementById("table-html-space");
    let htmlCode = document.getElementsByTagName("table")[0].innerHTML;


    htmlCode = document.getElementsByTagName("table")[0].innerHTML;
    textAreaSection.appendChild(textArea);
    textArea.append(htmlCode);

    textArea.style.width = "80%";
    textArea.style.height = "80%";

}

/**
 * code to call function when website is booting
 */
document.addEventListener('DOMContentLoaded', function () {
    let initialRows = document.getElementById("row-count");
    let initialCols = document.getElementById("col-count");
    document.addEventListener("blur", genTable(initialRows, initialCols));
});